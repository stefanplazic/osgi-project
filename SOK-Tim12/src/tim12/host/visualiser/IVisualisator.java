package tim12.host.visualiser;

import org.eclipse.zest.core.viewers.GraphViewer;

import tim12.host.source.ISource;

public interface IVisualisator {

	public static final String NAME_PROPERTY= "visualiser.visualisator";
	public static final String ICON_PROPERTY = "visualiser.icon";
	
	public GraphViewer getGraph(ISource source, GraphViewer viewer);//return the graphViewer to given Source
	public String getPluginName();
	public String getPluginID();
	
}
