package tim12.host.source;

import java.util.List;

import org.eclipse.swt.widgets.Display;

public interface ISource {

	public static final String NAME_PROPERTY ="source.name";
	public static final String ICON_PROPERTY = "source.image";

	public String getSourceName();
	public String getPluginID();//get the id of plugin witch immplements this interface
	public String getPluginName();// get the name of plugin witch immplements ISource
	public List<INode> getGraph();//returns a list of INodes to the visualisator
	public Object getDescription();//description of what this plug in does
	public void setDisplay(Display display);
}
