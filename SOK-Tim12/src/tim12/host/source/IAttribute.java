package tim12.host.source;

public interface IAttribute {
	public String getAttributeName();
	public String getAttributeValue();

}
