package tim12.host.source;

import java.util.List;

public interface IAttributeModel {

	public List<IAttribute> getAttributes();
}
