package tim12.host.source;

import java.util.List;

public interface INodeModel {

	public List<INode> getConnectedNodes();
}
