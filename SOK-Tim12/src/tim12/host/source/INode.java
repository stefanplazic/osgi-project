package tim12.host.source;

public interface INode {

	public String getTitle();
	public IAttributeModel getAttributes();//getting List of attributes for current node
	public INodeModel getNodes();//get all nodes witch are connected with this node
}
