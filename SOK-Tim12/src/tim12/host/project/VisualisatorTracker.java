package tim12.host.project;

import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import tim12.host.visualiser.IVisualisator;


public class VisualisatorTracker extends ServiceTracker{
	
	private BundleContext context;
	private MainWin window;
	
	private static final int ADD = 1;
	private static final int REMOVE = 2;
	private static final int MODIFY = 3;
	
	public VisualisatorTracker(BundleContext con, MainWin win)
	{
		super(con,IVisualisator.class.getName(),null);
		this.context = con;
		this.window = win;
		
		
	}


	@Override
	public Object addingService(ServiceReference reference) {
		//if(reference instanceof ISource)
		IVisualisator visualisator = (IVisualisator)context.getService(reference);
		processOnEventThread(ADD, reference, visualisator);
		return visualisator;
	}


	@Override
	public void modifiedService(ServiceReference reference, Object service) {
		// TODO Auto-generated method stub
		processOnEventThread(MODIFY, reference, (IVisualisator)service);
	}


	@Override
	public void removedService(ServiceReference reference, Object service) {
		// TODO Auto-generated method stub
		processOnEventThread(REMOVE, reference, (IVisualisator)service);
	}

	
	/*method for proccessing actions*/
	private void processAction(int action, ServiceReference ref, IVisualisator visualisator)
	{
		String name = (String)ref.getProperty(visualisator.NAME_PROPERTY);
		
		switch (action) {
		case ADD:

			window.addVisualisator(name, visualisator);
			break;

		case REMOVE:

			window.removeVisualisator(name);
			
			break;
		case MODIFY:

			break;
		}
	}
	
	private void processOnEventThread(
	        int action, ServiceReference ref, IVisualisator visualisator)
	    {
	        try
	        {
	            if (Display.getCurrent()!=null)
	            {
	               processAction(action, ref, visualisator);
	            }
	            else
	            {
	                Display.getDefault().asyncExec(new VisualisatorThread(action, ref, visualisator));
	            }
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	        }
	    }

	private class VisualisatorThread implements Runnable
	{
		private int action;
		private ServiceReference ref;
		private IVisualisator visualisator;
		
		
	
		public VisualisatorThread(int action, ServiceReference ref, IVisualisator visualisator) {
			super();
			this.action = action;
			this.ref = ref;
			this.visualisator = visualisator;
		}



		@Override
		public void run() {
			processAction(action, ref, visualisator);
			
		}
	
	
	}

}
