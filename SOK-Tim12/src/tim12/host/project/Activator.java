package tim12.host.project;

import java.awt.EventQueue;
import java.io.FileNotFoundException;
import java.util.HashMap;

import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import tim12.host.project.Activator;
import tim12.host.project.MainWin;
import tim12.host.project.SourceTracker;
import tim12.host.project.Activator.MyThread;
import tim12.host.source.ISource;

public class Activator implements BundleActivator,Runnable {

	private static BundleContext context;
	
	private MainWin window;
	private SourceTracker sTracker;
	private VisualisatorTracker vTracker;

	
	static BundleContext getContext() {
		return context;
	}
	
	

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		EventQueue.invokeLater(this);
		
		
		
		
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		//stop the source tracker
		sTracker.close();
		vTracker.close();
		//track.close();
	}

	@Override
	public void run() {
		try {
			window = new MainWin(new Display());
			SourceTracker tracker = new SourceTracker(context, window);
			tracker.open();
			
			vTracker = new VisualisatorTracker(context, window);
			vTracker.open();
			
			while (!window.shell.isDisposed()) {
			      if (!window.display.readAndDispatch()) {
			        window.display.sleep();
			      }
			    }
			    window.display.dispose();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
			
		
		
	}

	
	class MyThread extends Thread
	{
		
		@Override
		public void run() {
		Activator.this.run();
		}
		
	}
}
