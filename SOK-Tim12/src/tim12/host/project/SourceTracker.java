package tim12.host.project;

import javax.swing.Icon;
import javax.swing.SwingUtilities;


import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import tim12.host.source.ISource;

public class SourceTracker extends ServiceTracker{

	private BundleContext context;
	private MainWin window;
	
	private static final int ADD = 1;
	private static final int REMOVE = 2;
	private static final int MODIFY = 3;
	
	public SourceTracker(BundleContext con, MainWin win)
	{
		super(con,ISource.class.getName(),null);
		this.context = con;
		this.window = win;
		
		
	}


	@Override
	public Object addingService(ServiceReference reference) {
		//if(reference instanceof ISource)
		ISource source = (ISource)context.getService(reference);
		processOnEventThread(ADD, reference, source);
		return source;
	}


	@Override
	public void modifiedService(ServiceReference reference, Object service) {
		// TODO Auto-generated method stub
		processOnEventThread(MODIFY, reference, (ISource)service);
	}


	@Override
	public void removedService(ServiceReference reference, Object service) {
		// TODO Auto-generated method stub
		processOnEventThread(REMOVE, reference, (ISource)service);
	}

	
	/*method for proccessing actions*/
	private void processAction(int action, ServiceReference ref, ISource source)
	{
		String name = (String)ref.getProperty(source.NAME_PROPERTY);
		
		switch (action) {
		case ADD:
			
			
			
			window.addSource(name, source);
			break;

		case REMOVE:
			//remove the ISource to MainWin
			
			window.removeSource(name);
			
			break;
		case MODIFY:
			//modify the ISource to MainWin
			
			
			break;
		}
	}
	
	private void processOnEventThread(
	        int action, ServiceReference ref, ISource source)
	    {
	        try
	        {
	            if (Display.getCurrent()!=null)
	            {
	               processAction(action, ref, source);
	            }
	            else
	            {
	                Display.getDefault().asyncExec(new SourceThread(action, ref, source));
	            }
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	        }
	    }

	private class SourceThread implements Runnable
	{
		private int action;
		private ServiceReference ref;
		private ISource source;
		
		
	
		public SourceThread(int action, ServiceReference ref, ISource source) {
			super();
			this.action = action;
			this.ref = ref;
			this.source = source;
		}



		@Override
		public void run() {
			processAction(action, ref, source);
			
		}
	
	
	}
}
