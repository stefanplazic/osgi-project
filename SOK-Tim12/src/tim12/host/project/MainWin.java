package tim12.host.project;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import javax.swing.Icon;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.Graph;

import tim12.host.source.ISource;
import tim12.host.visualiser.IVisualisator;


public class MainWin {
	
	private HashMap<String,ISource> sources = new HashMap<String, ISource>();
	private HashMap<String,IVisualisator> visualisators = new HashMap<String,IVisualisator>();
	
	Display display ;
	Shell shell ;
	ToolBar toolBar;
	ToolBar tb;
	Menu menu,visualisatorMenu,sourceMenu;
	MenuItem fileMenuHeader;
	MenuItem visualistor, source;
	Menu fileMenu;
	Label label;
	GraphViewer gViewer;
	Graph graph;
	Composite c;
	
	//selected sources and visualisators
	ISource selectedS=null;
	IVisualisator selectedV=null;
	
	//listener for Visualistor Menu
	Listener vListener = new Listener() {
		
		@Override
		public void handleEvent(Event event) {
			//selection of visualistor menu itme
			MenuItem vItem = (MenuItem)event.widget;
			String vName = vItem.getText();
			selectedV = visualisators.get(vName);
			
			if(selectedS!=null & selectedV!=null){
				
				gViewer = selectedV.getGraph(selectedS, gViewer);
			}
			
		}
	};
	//listener for Source Menu
		Listener sListener = new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				//selection of visualistor menu itme
				MenuItem vItem = (MenuItem)event.widget;
				String vName = vItem.getText();
				
				selectedS = sources.get(vName);
				if(selectedS!=null & selectedV!=null){
					
					gViewer = selectedV.getGraph(selectedS, gViewer);
				}
			}
		};
	

	private void createMenuBar(Shell shell) {
		Menu menuBar = new Menu(shell, SWT.BAR);
	    shell.setMenuBar(menuBar);
	    
	    visualistor = new MenuItem(menuBar, SWT.CASCADE);
	    visualistor.setText("Visualisators");
	    //editItem.addListener(SWT.Arm, armListener);
	    
	    //sources
	     source = new MenuItem(menuBar, SWT.CASCADE);
	    source.setText("Sources");
	   	    
	    visualisatorMenu = new Menu(shell, SWT.DROP_DOWN);
	    visualistor.setMenu(visualisatorMenu);
	    
	   
	   sourceMenu = new Menu(shell,SWT.DROP_DOWN);
	   source.setMenu(sourceMenu);
	   
	   /*String[] editStrings = { "Source", "Source2"};
	    for (int i = 0; i < editStrings.length; i++) {
	      MenuItem item = new MenuItem(sourceMenu, SWT.PUSH);
	      item.setText(editStrings[i]);
	      //item.addListener(SWT.Arm, armListener);
	    }*/
	}
	
	private void createTabFolder(Shell shell) {
		
		 shell.setLayout(new FillLayout());
		 org.eclipse.swt.widgets.Canvas canvas = new org.eclipse.swt.widgets.Canvas(shell, SWT.None);
		 canvas.setLayout(new FillLayout());
	     canvas.setBackground(canvas.getDisplay().getSystemColor(SWT.COLOR_WHITE));
	     gViewer = new GraphViewer(canvas, SWT.BORDER);
		/*
		 final TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		 tabFolder.setSize(1050, 500);
		 
		 TabItem one = new TabItem(tabFolder, SWT.BORDER);
		 one.setText("Visualisation");
		 one.setToolTipText("This is graph");
		 
		 c = new Composite(tabFolder, SWT.BORDER);
	     c.setLayout(new FillLayout( ));
	     
	     one.setControl(c);
		 //one.setControl(getTabOneControl(tabFolder));
	
		 tabFolder.setSelection(1);

		 // Add an event listener to write the selected tab to stdout
		 tabFolder.addSelectionListener(new SelectionAdapter() {
		    public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
		        System.out.println(tabFolder.getSelection()[0].getText() + " selected");
		    }
		 });
		 */
	}
	
	public  void createToolBar(Shell shells) {
		System.out.println("Radi?");
        
		toolBar = new ToolBar(shell, SWT.NULL);
		toolBar.setBounds(0, 200, 1050, 64);
		
	    ToolItem itemPush = new ToolItem(toolBar, SWT.PUSH);
        itemPush.setText("Open");
        Image icon = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("xmlopenn.gif"));
	    itemPush.setImage(icon);
	    itemPush.setToolTipText("Open new source file");
	    
	    
	    ToolItem itemPush2 = new ToolItem(toolBar, SWT.PUSH);
	    itemPush2.setText(" Save ");
	    Image icon2 = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("xmlsaven.gif"));
	    itemPush2.setImage(icon2);
	    itemPush2.setToolTipText("Save");
	    
	    
	    ToolItem itemPush3 = new ToolItem(toolBar, SWT.PUSH);
	    itemPush3.setText("Delete");
	    Image icon3 = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("xmlclosen.gif"));
	    itemPush3.setImage(icon3);
	    itemPush3.setToolTipText("Delete file");
	    
	    
	    ToolItem itemPush4 = new ToolItem(toolBar, SWT.PUSH);
	    itemPush4.setText(" Help ");
	    Image icon4 = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("help.png"));
	    itemPush4.setImage(icon4);
	    itemPush4.setToolTipText("HELP");
	    
	    
	    ToolItem itemPush5 = new ToolItem(toolBar, SWT.PUSH);
	    itemPush5.setText(" About us ");
	    Image icon5 = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("aboutUs2.png"));
	    itemPush5.setImage(icon5);
	    
	    
	    Listener selectionListener = new Listener() {
	      public void handleEvent(Event event) {
	        ToolItem item = (ToolItem)event.widget;
	        System.out.println(item.getText() + " is selected");
	        if( (item.getStyle() & SWT.RADIO) != 0 || (item.getStyle() & SWT.CHECK) != 0 ) 
	          System.out.println("Selection status: " + item.getSelection());
	      }
	    };
	    
	    itemPush.addListener(SWT.Selection, selectionListener);
	    //itemPush2.addListener(SWT.Selection, selectionListener);
	    //itemPush3.addListener(SWT.Selection, selectionListener);
	    itemPush4.addListener(SWT.Selection, selectionListener);
	    
	    /*
	    Image toolBarLayout = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("images.jpg"));
	    toolBar.setBackgroundImage(toolBarLayout);

	    Image canvas_l = new Image(shell.getDisplay(), MainWin.class.getResourceAsStream("img.jpg"));
	    shell.setBackgroundImage(canvas_l);
	    */
	    
	    toolBar.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
	    
	    toolBar.pack();
	    
	    shell.addListener(SWT.Resize, new Listener() {
	      public void handleEvent(Event event) {
	        Rectangle clientArea = shell.getClientArea();
	        toolBar.setSize(toolBar.computeSize(clientArea.width, SWT.DEFAULT));
	      }
	    });
	    
	    
	   
	    shell.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent event) {
				Rectangle rect = shell.getClientArea();
				event.gc.drawOval(30, 90, rect.width -60, rect.height - 100);
			}
		});
		
	}
	/*add sources to meni*/
	private void manageSourceMenu(){
		
		MenuItem[] items = sourceMenu.getItems();
		
		//dispose of all menuItems
		for (MenuItem menuItem : items) {
			menuItem.dispose();
		}
		
		for (String key : sources.keySet()) {
			MenuItem item = new MenuItem(sourceMenu, SWT.PUSH);
			item.setText(key);
			//add listeners
			item.addListener(SWT.Selection, sListener);
		}
	}
	
		private void manageVisualisatorMenu(){
				
				MenuItem[] items = visualisatorMenu.getItems();
				
				for (MenuItem menuItem : items) {
					menuItem.dispose();
				}
				
				for (String key : visualisators.keySet()) {
					MenuItem item = new MenuItem(visualisatorMenu, SWT.PUSH);
					item.setText(key);
					//add listeners
					item.addListener(SWT.Selection, vListener);
				}
			}
	public MainWin(Display dispay) throws FileNotFoundException
	{
		this.display = dispay;
		this.shell = new Shell(dispay);
		
		shell.setLayout(new FillLayout());
		shell.setText("ExPreSsiVeNess");
		
		shell.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		
		createMenuBar(shell);
		//createToolBar(shell);
		createTabFolder(shell); 
	    
		
		shell.setSize(1050, 700);
		shell.open();
		
	    
	  }

	  
	
	  
	  /*add source*/
		public void addSource(String name, ISource source)
		{
			source.setDisplay(display);
			sources.put(name, source);
			//System.out.println(source.getDescription());
			manageSourceMenu();
		}

		public void removeSource(String name)
		{
			
			sources.remove(name);
			manageSourceMenu();
		}
		
		public void addVisualisator(String name, IVisualisator visualisator)
		{
			visualisators.put(name, visualisator);
			manageVisualisatorMenu();
		}

		public void removeVisualisator(String name)
		{
			
			visualisators.remove(name);
			manageVisualisatorMenu();
		}
			
			
		}	
	
	
	 

