package visualisator.visAdv;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.zest.core.viewers.IGraphEntityContentProvider;

import tim12.host.source.INode;
import tim12.host.source.ISource;



public class GraphContentProviderAdv implements IGraphEntityContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Object[] getElements(Object inputElement) {
		// TODO Auto-generated method stub
		if(inputElement instanceof ISource)
		{
			ISource source = (ISource)inputElement;	

			return source.getGraph().toArray();
		}
		else
			return new Object[]{inputElement};
	}
	
	@Override
	public Object[] getConnectedTo(Object entity) {
		// TODO Auto-generated method stub
		if(entity instanceof INode)
		{
			return ((INode) entity).getNodes().getConnectedNodes().toArray();
		}
		return null;
	}	

}
