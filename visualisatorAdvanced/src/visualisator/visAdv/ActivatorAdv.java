package visualisator.visAdv;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import tim12.host.visualiser.IVisualisator;


/**
 * The activator class controls the plug-in life cycle
 */
public class ActivatorAdv implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		ActivatorAdv.context = bundleContext;
		
		Hashtable<String, Object> table = new Hashtable<String,Object>();
		table.put(VisualisatorAdv.NAME_PROPERTY, "Visualisator2");
		ActivatorAdv.context.registerService(IVisualisator.class.getName(), new VisualisatorAdv(), table);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		ActivatorAdv.context = null;
	}

}