This project was done in course of learning about software design patterns and OSGI services. 
The project is done in java programming language using OSGI framework . The project helps us visualise  any source of graph type and shows it on panel.

There to main parts of project : the source and the visualiser. Source can be what ever user chooses he just needs to implement ISource interface in order for source to communicate with other services. 
User can also create it's own visualisers by implementing IVisualistor interface .  Visualisers using [ZEST](https://www.eclipse.org/gef/zest/) framework for drawing graphs.


FUTURE WORK:

That would be nice to add some other functionality to this app. Like the three structure of all available plugins and means to search it .
Adding feature for zoom in/out in order to see and analyze really big graphs.

All suggestions are welcomed :)  