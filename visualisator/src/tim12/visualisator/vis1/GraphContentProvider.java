package tim12.visualisator.vis1;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.zest.core.viewers.IGraphEntityContentProvider;

import tim12.host.source.INode;
import tim12.host.source.ISource;

public class GraphContentProvider implements IGraphEntityContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Object[] getElements(Object inputElement){
		// TODO Auto-generated method stub
		System.out.println(inputElement);
		if(inputElement instanceof ISource)
		{
			ISource source = (ISource)inputElement;	

			//System.out.println(source.getGraph().toArray());
			return source.getGraph().toArray();
		}
		return null;
		
	}
	
	@Override
	public Object[] getConnectedTo(Object entity) {
		// TODO Auto-generated method stub
		if(entity instanceof INode)
		{
			return ((INode) entity).getNodes().getConnectedNodes().toArray();
		}
		return null;
	}	

}
