package tim12.visualisator.vis1;

import org.eclipse.jface.viewers.LabelProvider;

import tim12.host.source.INode;

public class GraphLabelProvider extends LabelProvider {
	
	@Override
	public String getText(Object element) {
		if(element instanceof INode)
		{
			return ((INode)element).getTitle();
		}
		else if(element instanceof String)
		{
			System.out.println("Instanca je stringa " + element);
		}
		return "";
	}

}
