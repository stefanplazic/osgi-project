package tim12.visualisator.vis1;

import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;

import tim12.host.source.ISource;
import tim12.host.visualiser.IVisualisator;

public class VisualisatorVis implements IVisualisator {

	private String name = "Visualisator vis1";
	private String id = "visuelisator.vis1";
	
	@Override
	public GraphViewer getGraph(ISource source, GraphViewer viewer) {
		// TODO Auto-generated method stub
		viewer.getGraphControl().getConnections().clear();
		viewer.getGraphControl().getNodes().clear();
		viewer.getGraphControl().clear();
		
		viewer.setContentProvider(new GraphContentProvider());
		viewer.setLabelProvider(new GraphLabelProvider());
		viewer.setLayoutAlgorithm(new RadialLayoutAlgorithm());
		
		if (viewer.getInput() == null) {
			viewer.setConnectionStyle( ZestStyles.CONNECTIONS_DIRECTED);
		}
		viewer.setInput(source);
		
		return viewer;
	}

	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getPluginID() {
		// TODO Auto-generated method stub
		return id;
	}

}
