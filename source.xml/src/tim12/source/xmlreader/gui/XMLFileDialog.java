package tim12.source.xmlreader.gui;


import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

/**
 * This class demonstrates FileDialog
 */
public class XMLFileDialog {
  
  public String selectedFile;
  private Display display;
  
  public XMLFileDialog(Display display) {
		super();
		this.display = display;
	}


  private static final String[] FILTER_NAMES = {
      "XML Files (*.xml)"
    };

  private static final String[] FILTER_EXTS = { "*.xml", };

  /**
   * Runs the application
   */
  public void run() {
   
    Shell shell = new Shell(display);
    shell.setText("Choose XML file");
    createContents(shell);
    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    //display.dispose();
  }

 
  public void createContents(final Shell shell) {
    shell.setLayout(new GridLayout(5, true));

    new Label(shell, SWT.NONE).setText("File Name:");

    final Text fileName = new Text(shell, SWT.BORDER);
    GridData data = new GridData(GridData.FILL_HORIZONTAL);
    data.horizontalSpan = 4;
    fileName.setLayoutData(data);

    
    Button open = new Button(shell, SWT.PUSH);
    open.setText("Open");
  
    open.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        // User has selected to open a single file
        FileDialog dlg = new FileDialog(shell, SWT.OPEN);
        dlg.setFilterNames(FILTER_NAMES);
        dlg.setFilterExtensions(FILTER_EXTS);
        String fn = dlg.open();
        if (fn != null) {
          fileName.setText(fn);
          selectedFile= fn;
          
        }
      }
    });

  }

}
  
