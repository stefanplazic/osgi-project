package tim12.source.xmlreader.model;

import java.util.ArrayList;
import java.util.List;

import tim12.host.source.INode;
import tim12.host.source.INodeModel;

public class NodeModelXML implements INodeModel {
	
	
	private List<NodeXML> lista;

	public NodeModelXML(List<NodeXML> lista) {
		super();
		this.lista = lista;
	}


	@Override
	public List<INode> getConnectedNodes() {
		List<INode> nodovi =  new ArrayList<INode>();
		for(NodeXML node: lista){
			nodovi.add(node);
			
		}
		return nodovi;
	}
	
	

}
