package tim12.source.xmlreader.model;

import org.w3c.dom.Node;

import tim12.host.source.IAttribute;

public class AttributeXML implements IAttribute{

	NodeXML node;
	
	public AttributeXML(NodeXML n){
		node = n;
		
	}
	
	
	
	@Override
	public String getAttributeName() {
		return node.getNodeName();
	}

	@Override
	public String getAttributeValue() {
		return node.getNodeValue();
	}

}
