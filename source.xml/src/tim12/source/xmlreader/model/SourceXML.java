package tim12.source.xmlreader.model;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import tim12.host.source.INode;
import tim12.host.source.ISource;
import tim12.source.xmlreader.gui.XMLFileDialog;

public class SourceXML implements ISource{
	
	private final String PLUGIN_NAME="XML source";
	private final String PLUGIN_ID="source.xml";
	private final String PLUGIN_DESCRIPTION="Parsing XML file data and represents a graph ";
	private Display display;
	
	@Override
	public String getSourceName() {
		// TODO Auto-generated method stub
		return this.PLUGIN_NAME;
	}
	@Override
	public String getPluginID() {
		// TODO Auto-generated method stub
		return this.PLUGIN_ID;
	}
	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return this.PLUGIN_NAME;
	}
	@Override
	public List<INode> getGraph() {
		
		System.out.println("Dialog pozvan");
		//String path="C:/Users/Dunja/Desktop/projekatSOK/XMLParser/data/Order.xml";
		XMLFileDialog sfd = new XMLFileDialog(this.display);
		sfd.run();
		String path = sfd.selectedFile;
		System.out.println("Dialog pozvan 2");
	
		ParserXML parser = new ParserXML(path);
		System.out.println("Ucitan parser");
		parser.print();
		return parser.getNodeConnectedGraph();
		
	}
	@Override
	public Object getDescription() {
		// TODO Auto-generated method stub
		return this.PLUGIN_DESCRIPTION;
	}
	@Override
	public void setDisplay(Display d) {
			display = d;
		}

		
	}

