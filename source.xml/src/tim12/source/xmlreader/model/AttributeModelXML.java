package tim12.source.xmlreader.model;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;

import tim12.host.source.IAttribute;
import tim12.host.source.IAttributeModel;

public class AttributeModelXML  implements IAttributeModel{

	NodeXML node;
	
	public AttributeModelXML(NodeXML n){
		node= n;	
	}
	
	@Override
	public List<IAttribute> getAttributes() {
		List <IAttribute> attrs = new ArrayList<IAttribute>();
        AttributeXML xmla = new AttributeXML(node);
        attrs.add(xmla);
        
		return attrs;
	}
	
	
	
	
	
	
	

}
