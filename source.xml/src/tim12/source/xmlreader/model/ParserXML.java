package tim12.source.xmlreader.model;

//This file demonstates a simple use of the parser and DOM API.
//The XML file that is given to the application is parsed and the
//elements and attributes in the document are printed.
//The use of setting the parser options is demonstrated.
//

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import tim12.host.source.INode;


public class ParserXML
{
	 List<NodeXML> xmll = new ArrayList<NodeXML>();
	 String xml_file;
	
	 public ParserXML(String file){
		 xml_file =file;
		 load(); 
		}
	 
	public void load()
	 {
     	try
	 {
    
      // Get an instance of the parser
      DOMParser dparser = new DOMParser();
	
      
	 URL url = createURL(xml_file);
     
      dparser.setErrorStream(System.err);
      dparser.setValidationMode(dparser.NONVALIDATING);
      dparser.showWarnings(true);
	
     dparser.parse(url);
     
      
      XMLDocument doc = dparser.getDocument();
      
      NodeList nl = doc.getElementsByTagName("*");

      for (int i=0; i<nl.getLength(); i++)
      {
	   	  Node n;
	      n = nl.item(i);
	      NodeXML xn = new NodeXML(n, i);
	    
	      xmll.add(xn);
	      
		     
      }

      
       
     
   }
   catch (Exception e)
   {
      System.out.println(e.toString());
   }
}

	
public void print(){
	NodeXML x;
	for (int i=0; i<xmll.size();i++){
		x=xmll.get(i);
		x.printNode();
	}
}
	
NodeXML findById (int id){
	NodeXML x;
	for (int i=0; i<xmll.size();i++){
		if (xmll.get(i).getId()==id){
			x=xmll.get(i);
		    return x;
		}
	}
	return null;
	
}
	 

static URL createURL(String fileName)
{
   URL url = null;
   try 
   {
      url = new URL(fileName);
   } 
   catch (MalformedURLException ex) 
   {
      File f = new File(fileName);
      try 
      {
         String path = f.getAbsolutePath();
         String fs = System.getProperty("file.separator");
         if (fs.length() == 1)
         {
            char sep = fs.charAt(0);
            if (sep != '/')
               path = path.replace(sep, '/');
            if (path.charAt(0) != '/')
               path = '/' + path;
         }
         path = "file://" + path;
         url = new URL(path);
      } 
      catch (MalformedURLException e) 
      {
         System.out.println("Cannot create url for: " + fileName);
         System.exit(0);
      }
   }
   return url;
}

public List<INode> getNodeConnectedGraph() {
	final List<INode> xmllist = new ArrayList<INode>();
	
	for (NodeXML node : xmll)
	{
		xmllist.add(node);
		
	}
	return xmllist;
}

/*
 * testiranje
 * 
public static void main(String[] arg){
	XmlParser p = new XmlParser("data/Order.xml");
	List<XmlNode> l = p.load();
	XmlNodeModel model = new XmlNodeModel(l);
	
	
	
	for (int i=0; i<model.getConnectedNodes().size();i++){
		XmlNode n = model.getConnectedNodes().get(i);
		n.printNode();	
		}
	
	// Pronadji nod po id
	XmlNode xx = p.findById(3);
	xx.printNode();
    
   }
*/
}
