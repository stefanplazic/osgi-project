package tim12.source.xmlreader.model;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tim12.host.source.IAttributeModel;
import tim12.host.source.INode;
import tim12.host.source.INodeModel;

public class NodeXML implements INode{

	boolean hasChildren;
	   Node n;
	   int node_type;
	   String node_name;
	   String node_value;
	   int id;
	   
	   
   public NodeXML(Node nn, int idd){
	   n=nn;
	   node_type=nn.getNodeType();
	   node_name=nn.getNodeName();
	   node_value=nn.getNodeValue();
	   hasChildren = nn.hasChildNodes();
	   
	   id=idd;
   }
   
   
   
   public int getId(){
	   return id;
   }
   
   public String getNodeValue(){
	
	   node_value=printNodeValue();
	   return node_value;
   }
   
   Node getParentNode() {
		return n.getParentNode();
	    }   
   
   NodeList getChildren(){
	   if (hasChildren)
	   	  return n.getChildNodes();
	return null;
   }
   
   public String getNodeName(){
	   return node_name;
   }
   
   
   public void printNode(){
			   System.out.println("Node_id:"+getId() +" Parent:"+getParentNode().getNodeName() +" Has_children: " +hasChildren + " Node_type:"+getNodeType() + " Node_name:"+getNodeName()+" Node_value:"+getNodeValue());
		 
   }
   
  
   
   
   public int getNodeType(){
	   
	   return node_type;
   }
   
   
   public String printNodeValue() {
	    StringBuffer result = new StringBuffer();
	    
	  
	    
	    if (! hasChildren){
	   
		   return result.toString(); 
	    }

	    NodeList list = getChildren();
	    
	  
	    
	    for (int i=0; i < list.getLength(); i++) {
	    	
	        Node subnode = list.item(i);
	     /*
	        if (subnode.getNodeType() == Node.ELEMENT_NODE) {
	        	NodeList t = getChildren();
	        	result.append("TEXT_NODE:"+subnode.getNodeValue());
		         // 
	        
	        }
	        else*/
	        if (subnode.getNodeType() == Node.TEXT_NODE) {
	            result.append(subnode.getNodeValue());
	            
	        }
	        else if (subnode.getNodeType() == Node.CDATA_SECTION_NODE) {
	            result.append("CDATA_SECTION_NODE"+subnode.getNodeValue());
	        }
	       
	    }

	    return result.toString();
	}



@Override
public String getTitle() {
	// TODO Auto-generated method stub
	return node_name;
}



@Override
public IAttributeModel getAttributes() {
	
	
	AttributeModelXML xma = new AttributeModelXML(this);
	return xma;
	
	//return (IAttributeModel) n.getAttributes();
	
}



@Override
public INodeModel getNodes() {
	// TODO Auto-generated method stub
	@SuppressWarnings("unchecked")
	List<NodeXML> lst = new ArrayList<NodeXML>();
	NodeList list =   n.getChildNodes();
	
	for (int i=0; i< list.getLength();i++) {
		Node it = list.item(i);
		NodeXML nXMl = new NodeXML(it, i);
		lst.add(nXMl);
	}
	
	NodeModelXML xmlnm = new NodeModelXML(lst);
	xmlnm.getConnectedNodes();
	return xmlnm;
}
   
}
