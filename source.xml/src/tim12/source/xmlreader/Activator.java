package tim12.source.xmlreader;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import tim12.host.source.ISource;
import tim12.source.xmlreader.model.SourceXML;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		Hashtable<String, Object> xml_files = new Hashtable<String,Object>();
		xml_files.put(SourceXML.NAME_PROPERTY, "XML Parser");
	    Activator.context.registerService(ISource.class.getName(), new SourceXML(), xml_files);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
