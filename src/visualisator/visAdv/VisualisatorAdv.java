package visualisator.visAdv;

import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;

import source.ISource;
import visualiser.IVisualisator;

public class VisualisatorAdv implements IVisualisator {

	private String name = "Visualisator visAdv";
	private String id = "visuelisator.visAdv";
	
	@Override
	public GraphViewer getGraph(ISource source, GraphViewer viewer) {
		// TODO Auto-generated method stub
		viewer.getGraphControl().getConnections().clear();
		viewer.getGraphControl().getNodes().clear();
		//viewer.getGraphControl().clear();
		
		viewer.setContentProvider(new GraphContentProviderAdv());
		viewer.setLabelProvider(new GraphLabelProviderAdv());
		viewer.setLayoutAlgorithm(new RadialLayoutAlgorithm());
		
		if (viewer.getInput() == null) {
			viewer.setConnectionStyle( ZestStyles.CONNECTIONS_DIRECTED);
		}
		viewer.setInput(source);
		
		return viewer;
	}

	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getPluginID() {
		// TODO Auto-generated method stub
		return id;
	}

}
