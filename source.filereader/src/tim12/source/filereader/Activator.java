package tim12.source.filereader;

import java.util.Hashtable;
import java.util.List;

import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import tim12.host.source.IAttribute;
import tim12.host.source.IAttributeModel;
import tim12.host.source.INode;
import tim12.host.source.ISource;
import tim12.source.filereader.gui.FilePicker;
import tim12.source.filereader.model.FileNode;
import tim12.source.filereader.model.FileSource;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		
		Hashtable<String, Object> table = new Hashtable<String,Object>();
		table.put(FileSource.NAME_PROPERTY, "DirectoryReader");
		Activator.context.registerService(ISource.class.getName(), new FileSource(), table);
		
		
		
				
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
