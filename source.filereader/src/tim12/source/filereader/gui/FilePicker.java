package tim12.source.filereader.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class FilePicker {

	public String selectedFile;
	
	private Display display;
	
	
	  
	  public FilePicker(Display display) {
		super();
		this.display = display;
	}

	public void run() {
		
	    Shell shell = new Shell(display);
	    shell.setText("Choose directory");
	    createContents(shell);
	    shell.pack();
	    shell.open();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch()) {
	        display.sleep();
	      }
	    }
	    //display.dispose();
	  }

	  /**
	   * Creates the contents for the window
	   * 
	   * @param shell the parent shell
	   */
	  public void createContents(final Shell shell) {
	    shell.setLayout(new GridLayout(5, true));

	    new Label(shell, SWT.NONE).setText("Directory Name:");

	    final Text fileName = new Text(shell, SWT.BORDER);
	    GridData data = new GridData(GridData.FILL_HORIZONTAL);
	    data.horizontalSpan = 4;
	    fileName.setLayoutData(data);

	    
	    Button open = new Button(shell, SWT.PUSH);
	    open.setText("Open");
	  
	    open.addSelectionListener(new SelectionAdapter() {
	      public void widgetSelected(SelectionEvent event) {
	        // User has selected to open a single file
	        DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
	        
	        String fn = dlg.open();
	        if (fn != null) {
	          fileName.setText(fn);
	          selectedFile= fn;
	          
	        }
	      }
	    });

	  }
	
	
}
