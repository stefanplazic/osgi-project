package tim12.source.filereader.model;

import java.util.ArrayList;
import java.util.List;

import tim12.host.source.INode;
import tim12.host.source.INodeModel;

public class FileNodes implements INodeModel {

	private List<INode> nodes;
	
	public FileNodes() {
		nodes = new ArrayList<INode>();
	}
	
	public void addNode(FileNode node){
		nodes.add(node);
	}
	
	@Override
	public List<INode> getConnectedNodes() {
		// TODO Auto-generated method stub
		return nodes;
	}

}
