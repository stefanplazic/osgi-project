package tim12.source.filereader.model;

import java.util.ArrayList;
import java.util.List;

import tim12.host.source.IAttribute;
import tim12.host.source.IAttributeModel;

public class FileAttributList implements IAttributeModel {

	private List<IAttribute> attrs;
	
	public FileAttributList() {
		attrs = new ArrayList<IAttribute>();
	}
	
	
	//add attributes
	public void addAttr(FileAttribute att)
	{
		this.attrs.add(att);
		
	}
	
	@Override
	public List<IAttribute> getAttributes() {
		// TODO Auto-generated method stub
		return attrs;
	}

}
