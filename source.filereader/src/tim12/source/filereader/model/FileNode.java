package tim12.source.filereader.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import tim12.host.source.IAttributeModel;
import tim12.host.source.INode;
import tim12.host.source.INodeModel;

public class FileNode implements INode {

	private String fileName="";
	private IAttributeModel attrs;
	private INodeModel connectedNodes;
	
	/*static class for getting all files inside directorium*/
	
	public static  FileNode getDirFiles(File dir){
		FileNode node = new FileNode(dir.getName());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		node.addAttribute(new FileAttribute("date modifed", sdf.format(dir.lastModified())));
		if(!dir.isDirectory())
		{
			node.addAttribute(new FileAttribute("type:", "file"));
			return node;
		}
		
		node.addAttribute(new FileAttribute("type:", "directory"));
		for (File file : dir.listFiles()) {
			 node.addConnectedNode( FileNode.getDirFiles(file));
		}
		return node;
		
	}
	
	
	public FileNode(String fileName) {
		super();
		this.fileName = fileName;
		this.attrs = new FileAttributList();
		this.connectedNodes= new FileNodes();
		
	}
	
	public void addAttribute(FileAttribute at){
		
		((FileAttributList)attrs).addAttr(at);
	}
	
	public void addConnectedNode(FileNode n){
		((FileNodes)connectedNodes).addNode(n);
		
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return fileName;
	}

	@Override
	public IAttributeModel getAttributes() {
		// TODO Auto-generated method stub
		return attrs;
	}

	@Override
	public INodeModel getNodes() {
		// TODO Auto-generated method stub
		return connectedNodes;
	}

}
