package tim12.source.filereader.model;

import tim12.host.source.IAttribute;

public class FileAttribute implements IAttribute {

	private String name;
	private String value;
	
	
	
	public FileAttribute(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@Override
	public String getAttributeName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getAttributeValue() {
		// TODO Auto-generated method stub
		return value;
	}

}
