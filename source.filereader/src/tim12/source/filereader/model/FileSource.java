package tim12.source.filereader.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.eclipse.swt.widgets.Display;

import tim12.host.source.INode;
import tim12.host.source.ISource;
import tim12.source.filereader.gui.FilePicker;

public class FileSource implements ISource {

	private String pluginName="file parser";
	private String pluginID="file.parser";
	private String description="shows structure of folders";
	private String path="";
	private Display display;
	public FileSource() {
		// TODO Auto-generated constructor stub
	}
	
	
	


	@Override
	public String getSourceName() {
		// TODO Auto-generated method stub
		return pluginName;
	}

	@Override
	public String getPluginID() {
		// TODO Auto-generated method stub
		return pluginID;
	}

	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return pluginName;
	}

	@Override
	public List<INode> getGraph() {
		FilePicker picker = new FilePicker(this.display);
		picker.run();
		this.path= picker.selectedFile;	
		System.out.println(path);
		List<INode> n = new ArrayList<INode>();
		if(this.path==null)
			return n;
		try{		 		
		File file = new File(path);
		if(file.exists())
		{
			for (File f : file.listFiles()) 
				n.add(FileNode.getDirFiles(f));						
		}
																		
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
				
		return n;
	}
	
	 

	@Override
	public Object getDescription() {
		// TODO Auto-generated method stub
		return description;
	}





	@Override
	public void setDisplay(Display display) {
		// TODO Auto-generated method stub
		this.display = display;
	}

}
